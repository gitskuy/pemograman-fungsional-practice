data Car a b c = Car { company :: a
                     , model :: b
                     , year :: c
                     } deriving (Show)



tellCar :: Car -> String
tellCar (Car {company = c, model = m, year = y}) = "This " ++ c ++ " " ++ m ++ " was made in " ++ show y

stang = Car {company="Ford", model="Mustang", year=1967}
--- >>> tellCar stang
--- <interactive>:1194:2: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: tellCar :: t0 -> IO a0
--- <BLANKLINE>
--- <interactive>:1194:10: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: stang
--- *** Exception: <interactive>:1194:2: error:
---     Variable not in scope: tellCar :: t0 -> IO a0
--- (deferred type error)
---
--- >>> tellCar (Car "Ford" "Mustang" 1967)
--- <interactive>:1190:2: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: tellCar :: t0 -> IO a0
--- <BLANKLINE>
--- <interactive>:1190:11: warning: [-Wdeferred-out-of-scope-variables]
---     Data constructor not in scope:
---       Car :: [Char] -> [Char] -> Integer -> t0
--- *** Exception: <interactive>:1190:2: error:
---     Variable not in scope: tellCar :: t0 -> IO a0
--- (deferred type error)
---
--- >>> :t Car "Ford" "Mustang" "nineteen sixty seven"
--- <interactive>:1:22: warning: [-Wdeferred-type-errors]
---     • Couldn't match expected type ‘Int’ with actual type ‘[Char]’
---     • In the third argument of ‘Car’, namely ‘"nineteen sixty seven"’
---       In the expression: Car "Ford" "Mustang" "nineteen sixty seven"
--- Car "Ford" "Mustang" "nineteen sixty seven" :: Car
---
