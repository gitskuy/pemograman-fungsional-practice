--- Lets learn modules !!

--- modules merupakan collection dari fungsi yang ber-relas
---- haskell starand librar di bagi menjadi module untuk import contoh dibawah

---- Import modules

import Data.List
import Geometry.Sphere()


numUniq :: (Eq a) => [a] -> Int
numUniq = length . nub

--- >>> intersperse '.' "MONKEY"
--- "M.O.N.K.E.Y"
--- >>> numUniq [1,2,3]
--- 3
---

--- >>> Geometry.Sphere 4
--- <interactive>:2087:2: error:
---     Not in scope: data constructor ‘Geometry.Sphere’
---     No module named ‘Geometry’ is imported.
---
