applyMaybe :: Maybe a -> (a -> Maybe b) -> Maybe b
applyMaybe Nothing f  = Nothing
applyMaybe (Just x) f = f x

--- >>> Just 3 `applyMaybe` \x -> Just(x+1)
--- Just 4
--- >>> Nothing `applyMaybe` \x -> Just (x+1)
--- Nothing
--- >>> Just 4 `applyMaybe` \x -> Just (x+1)
--- Just 5
--- >>> Just 1 `applyMaybe` \x -> if x > 2 then Just x else Nothing
--- Nothing
--- >>> Just 3 `applyMaybe` \x -> if x > 2 then Just x else Nothing
--- Just 3
---


---- Finaly Starting Monads ----
class Monad m where
    return :: a -> m a

    (>>=) :: m a -> (a -> m b) -> m b

    (>>) :: m a -> m b -> m b
    x >> y = x >>= \_ -> y

    fail :: String -> m a
    fail msg = error msg

--- Note : Maybe is an instance of monad ---
instance Monad Maybe where
    return x = Just x
    Nothing >>= f = Nothing
    Just x >>= f  = f x
    fail _ = Nothing

--- >>> return "What" :: Maybe String
--- Just "What"
--- >>> Just 9 >>= \x -> return (x*10)
--- Just 90
---


type Birds = Int
type Pole = (Birds,Birds)

landLeft :: Birds -> Pole -> Pole
landLeft n (left,right) = (left + n,right)

landRight :: Birds -> Pole -> Pole
landRight n (left,right) = (left,right + n)

--- >>> landRight (-1) (1,2)
--- <interactive>:1299:2: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope:
---       landRight :: Integer -> (Integer, Integer) -> IO a0
--- *** Exception: <interactive>:1299:2: error:
---     Variable not in scope:
---       landRight :: Integer -> (Integer, Integer) -> IO a0
--- (deferred type error)
---

x -: f = f x

--- >>> 100 -: (*3)
