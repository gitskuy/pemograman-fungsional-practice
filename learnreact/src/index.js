import React, { Component } from 'react';
import { render } from 'react-dom';
import './style.css';

// If you classes in React, this code should look familiar:

// Components
import Greeting from './GreetingsFungtional';
import Counter from './Counter';
import Kendoka from './Kendoka';
import Calc from './calculatorfrontend'

class App extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className="wrapper">
        <Calc />
        <br></br>
        <Kendoka />
        <br></br>
        <Greeting />
        <br></br>
        <Counter />

      </div>
    );
  }
}

render(<App />, document.getElementById('root'));
