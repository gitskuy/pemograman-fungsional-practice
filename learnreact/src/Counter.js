import React, { Component,useState ,useEffect } from 'react';

const Counter = () => {
  const [count, setCount] = useState(() =>
    parseInt(window.localStorage.getItem('count')) || 0
  );
  // const [count, setCount] = useState(0);
  const incrementCount = () => setCount(count + 1);
  const decrementCount = () => setCount(count - 1);

  useEffect(() => {
    document.title = `You clicked ${count} times`
    window.localStorage.setItem('count',count), count;
  });

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={incrementCount}>Click Me To Increment</button>
      <button onClick={decrementCount}>Click Me To Decrement</button>
    </div>
  )
}

export default Counter;
