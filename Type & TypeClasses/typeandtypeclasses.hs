

--- Type And Typeclasees

factorial :: Integer -> Integer
factorial n = product [1..n]

--- >>> factorial 50
--- 30414093201713378043612608166064768844377641568960512000000000000
---

circumfrence :: Float -> Float
circumfrence r = 2 * pi * r

--- >>> circumfrence 4.0
--- 25.132742
---

circumfrenceDouble :: Double -> Double
circumfrenceDouble r = 2 * pi * r

--- >>> circumfrenceDouble 4.0
--- 25.132741228718345
---

youGood :: Char -> Bool
youGood x = False

--- >>> youGood 'a'
--- False
---


--- Typeclasees

--- >>> :t (==)
--- (==) :: Eq a => a -> a -> Bool
--- a is generic  " Because it's not in capital case it's actually a type variable. That means that a can be of any type. This is much like generics in other languages, only in Haskell it's much more powerful because it allows us to easily write very general functions if they don't use any specific behavior of the types in them. Functions that have type variables are called polymorphic functions"
--- Eq is used for types that support equality testing.

--- >>> :t fst
--- fst :: (a, b) -> a
---


--- >>> :t (>)
--- (>) :: Ord a => a -> a -> Bool
--- "Ord covers all the standard comparing functions such as >, <, >= and <=. The compare function takes two Ord members of the same type and returns an ordering. Ordering is a type that can be GT, LT or EQ, meaning greater than, lesser than and equal, respectively."

--- >>> "Abrakadabra" < "Zebra"
--- True
---

--- >>> show 5
--- "5"
--- >>> read "5" + 4
--- 9
---

--- >>> :t read
--- read :: Read a => String -> a
---

--- >>> minBound :: Int
--- -9223372036854775808


--- >>> maxBound :: (Bool,Int,Char)
--- (True,9223372036854775807,'\1114111')
---

---- >>> :t 20
---- 20 :: Num t => t

--- >>> 20 :: Int
--- 20
---

--- >>> 20 :: Integer
--- 20
---

--- >>> 20 :: Float
--- 20.0
---

--- >>> 20 :: Double
--- 20.0
---

--- >>> :t (*)
--- (*) :: Num a => a -> a -> a
---

--- >>>  (5 :: Int) * (6 :: Integer)
--- <interactive>:389:17: error:
---     • Couldn't match expected type ‘Int’ with actual type ‘Integer’
---     • In the second argument of ‘(*)’, namely ‘(6 :: Integer)’
---       In the expression: (5 :: Int) * (6 :: Integer)
---       In an equation for ‘it’: it = (5 :: Int) * (6 :: Integer)
--- >>> (5 :: Int) * 6
--- 30
---

--- Integral is also a numeric typeclass. Num includes all numbers, including real numbers and integral numbers, Integral includes only integral (whole) numbers. In this typeclass are Int and Integer.

--- Floating includes only floating point numbers, so Float and Double.
