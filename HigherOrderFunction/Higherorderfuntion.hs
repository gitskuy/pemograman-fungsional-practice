multThree :: (Num a) => a -> a -> a -> a
multThree x y z = x * y * z

--- >>> multThree 2 2 2
--- 8
---

---- all higher order functions actually recursivly doing the function

--- >>> let multTwoWithNine = multThree 9
--- >>> multTwoWithNine 2 3
--- 54
---

compareWithHundred :: (Num a,Ord a) => a -> Ordering
compareWithHundred x = compare 100 x

--- >>> compareWithHundred 100
--- EQ
---

divideByTen :: (Floating a) => a -> a
divideByTen = (/10)

--- >>> divideByTen 2
--- 0.2
---

--- >>> multThree 3 4
--- <interactive>:1616:2: error:
---     • No instance for (Show (a0 -> a0)) arising from a use of ‘print’
---         (maybe you haven't applied a function to enough arguments?)
---     • In a stmt of an interactive GHCi command: print it
---

--- takes in a function and an interger or string.
applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)


--- >>> applyTwice (multThree 2 3) 10
--- 360
--- >>> applyTwice (++ " HAHA") "HEY"
--- "HEY HAHA HAHA"
---  >>>:t applyTwice
---  applyTwice :: (a -> a) -> a -> a
--- >>> applyTwice ("HAHA " ++) "HEY"
--- "HAHA HAHA HEY"
---


--- takes in  one function and two list
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

--- >>> zipWith' (+) [4,2,5,6] [2,6,2,3]
--- [6,8,7,9]
--- >>>  zipWith' (++) ["foo ", "bar ", "baz "] ["fighters", "hoppers", "aldrin"]
--- ["foo fighters","bar hoppers","baz aldrin"]
---

flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = g
  where g x y = f y x

---- >>> flip' zip [1,2,3,4,5] "What up"
---- [('W',1),('h',2),('a',3),('t',4),(' ',5)]
---- >>> zipWith (flip' div) [2,2..] [10,8,6,4,2]
---- [5,4,3,2,1]
---- >>> flip zip [1,2,3,4,5] "What up"
---- [('W',1),('h',2),('a',3),('t',4),(' ',5)]
----


--- Reading the type declaration, we say that it takes a function that takes an a and a b and returns a function that takes a b and an a. But because functions are curried by default, the second pair of parentheses is really unnecessary, because -> is right associative by default. (a -> b -> c) -> (b -> a -> c) is the same as (a -> b -> c) -> (b -> (a -> c)), which is the same as (a -> b -> c) -> b -> a -> c. We wrote that g x y = f y x. If that's true, then f y x = g x y must also hold, right? Keeping that in mind, we can define this function in an even simpler manner.

--- Maps and Filters ---


--- mapz take function and list and apply that funtion to every element on that list!
mapz :: (a -> b) -> [a] -> [b]
mapz _ [] = []
mapz f (x:xs) = f x : mapz f xs

--- >>> mapz (+3) [1,2,3,4,5]
--- [4,5,6,7,8]
--- >>> map (+10) [2,4,6,8]
--- [12,14,16,18]
--- >>> map (++ " Appreciate ") ["Who","Do","WE"]
--- ["Who Appreciate ","Do Appreciate ","WE Appreciate "]
--- >>> mapz (replicate 4) [1 .. 50]
--- [[1,1,1,1],[2,2,2,2],[3,3,3,3],[4,4,4,4],[5,5,5,5],[6,6,6,6],[7,7,7,7],[8,8,8,8],[9,9,9,9],[10,10,10,10],[11,11,11,11],[12,12,12,12],[13,13,13,13],[14,14,14,14],[15,15,15,15],[16,16,16,16],[17,17,17,17],[18,18,18,18],[19,19,19,19],[20,20,20,20],[21,21,21,21],[22,22,22,22],[23,23,23,23],[24,24,24,24],[25,25,25,25],[26,26,26,26],[27,27,27,27],[28,28,28,28],[29,29,29,29],[30,30,30,30],[31,31,31,31],[32,32,32,32],[33,33,33,33],[34,34,34,34],[35,35,35,35],[36,36,36,36],[37,37,37,37],[38,38,38,38],[39,39,39,39],[40,40,40,40],[41,41,41,41],[42,42,42,42],[43,43,43,43],[44,44,44,44],[45,45,45,45],[46,46,46,46],[47,47,47,47],[48,48,48,48],[49,49,49,49],[50,50,50,50]]
---


filterz :: (a -> Bool) -> [a] -> [a]
filterz _ [] = []
filterz p (x:xs)
    | p x = x : filterz p xs
    | otherwise = filterz p xs

--- >>> filterz (<15) [1,2,3,4,5]
--- [1,2,3,4,5]
--- >>> filter even [1..69]
--- [2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,68]
--- >>> filter (`elem` ['a'..'z']) "u LaUgH aT mE BeCaUsE I aM diFfeRent"
--- "uagameasadifeent"
--- >>> filterz (`elem` ['A'..'Z']) "i lauGh At You BecAuse u r aLL the Same"
--- "GAYBALLS"
---

quicksortz :: (Ord a) => [a] -> [a]
quicksortz [] = []
quicksortz (x:xs) =
  let smallerSorted = quicksortz (filterz (<=x) xs)
      biggerSorted = quicksortz (filterz (>x) xs)
  in smallerSorted ++ [x] ++ biggerSorted

--- >>> quicksortz [2,3,4,2]
--- [2,2,3,4]
---

--- take number shat is divisble by 3829
largestDivisible :: (Integral a) => a
largestDivisible = head (filter p [1000000,999999..])
  where p x = x `mod` 3829 == 0

--- >>> largestDivisible
--- 999369
---

--- >>>  sum (takeWhile (<10000) (filter odd (map (^2) [1..])))
--- 166650
---

--- >>> sum (takeWhile (<10000) (filter odd (map (^2) [1..])))
--- 166650
---

chainz :: (Integral a) => a -> [a]
chainz 1 = [1]
chainz n
    | even n =  n:chainz (n `div` 2)| odd n  =  n:chainz (n*3 + 1)
--- >>> chainz 10
--- [10,5,16,8,4,2,1]
---

numLongChains :: Int
numLongChains = length (filter isLong (map chainz [1..100]))
    where isLong xs = length xs > 15

--- >>> numLongChains
--- 66
--- >>> let listOfFuns = map (*) [0..]
--- >>> (listOfFuns !! 4) 5
--- 20
---

numLongChainz :: Int
numLongChainz = length (filter (\xs -> length xs > 15) (map chainz[1..100]))

--- Lambdas are basically anonymous functions that are used because we need some functions only once.

--- >>> numLongChainz
--- 66

--- >>> zipWith (\a b -> (a * 30 + 3) / b) [5,4,3,2,1] [1,2,3,4,5]
--- [153.0,61.5,31.0,15.75,6.6]
---
addThree :: (Num a) => a -> a -> a -> a
addThree x y z = x + y + z

addThreeWlambda :: (Num a) => a -> a -> a -> a
addThreeWlambda = \x -> \y -> \z -> x + y + z

--- >>> addThree 4 5 6
--- >>> addThreeWlambda 10 9 10
--- 15
--- 29
---


flipwlambda :: (a -> b -> c) -> b -> a -> c
flipwlambda f = \x y -> f y x

--- >>> flipwlambda zip "Hellow" [1..100]
--- [(1,'H'),(2,'e'),(3,'l'),(4,'l'),(5,'o'),(6,'w')]
---

sumwfold :: (Num a) => [a] -> a
sumwfold xs = foldl (\acc x -> acc + x) 0 xs

--- >>> sumwfold [4,5,6,2,1]
--- 18
--- fold list dari kiri dengan akumulator yang melipat setiap kali melakukan sebuah pertambahan

sumwfold2 :: (Num a) => [a] -> a
sumwfold2 = foldl (+) 0

--- >>> sumwfold2 [4,1,3,2]
--- 10
---

checkdidalam :: (Eq a) => a -> [a] -> Bool
checkdidalam y ys = foldl (\acc x -> if x == y then True else acc) False ys

--- >>> checkdidalam 5 [1,2,3,4,5]
--- True
---
--- >>> elem 4 [1,2,3]

--- foldr mirim foldf namun accumulator jalan dari kanan

sumwfoldr :: (Num a) => [a] -> a
sumwfoldr = foldr (+) 0

--- >>> sumwfoldr [4,3,2,1]
--- 10
---


mapwfoldr :: (a -> b) -> [a] -> [b]
mapwfoldr f xs = foldr (\x acc -> f x : acc) [] xs

--- >>> mapwfoldr (+3) [1,2,3]
--- [4,5,6]
---



maximumwfold :: (Ord a) => [a] -> a
maximumwfold =  foldr1 (\x acc -> if x > acc then x else acc)

--- >>> maximumwfold [1,2,3,4]
--- 4
--- Foldr1 doesnt need starting value ! like magic

sumwfoldr1 :: (Num a) => [a] -> a
sumwfoldr1 = foldr1 (+)

--- >>> sumwfoldr1 [4,2,3,1]
--- 15
---


filterwfoldr1 :: (a -> Bool) -> [a] -> [a]
filterwfoldr1 p = foldr (\x acc -> if p x then x : acc else acc) []

--- >>> filterwfoldr1 (>1) [1,2,3,4]
--- [2,3,4]
---

reverdale :: [a] -> [a]
reverdale = foldl (\acc x -> x : acc) []

--- >>> reverdale [1,2,3]
--- [3,2,1]
---


lastl :: [a] -> a
lastl = foldl1 (\_ x -> x)

--- >>> lastl [1,3,4]
--- 4
---


--- scan? mirip fold namun return list

--- >>> scanl (+) 0 [3,5,2,1]
--- [0,3,8,10,11]
--- >>> scanl (flip (:)) [] [3,2,1]
--- [[],[3],[2,3],[1,2,3]]
---


sqrtSums :: Int
sqrtSums = length (takeWhile (<1000) (scanl1 (+) (map sqrt [1..]))) + 1

--- >>> sqrtSums
--- 131
---
 --- function application with $


--- >>> sqrt - 3 + 4 + 9
--- <interactive>:27:2: error:
---     • Non type-variable argument in the constraint: Num (a -> a)
---       (Use FlexibleContexts to permit this)
---     • When checking the inferred type
---         it :: forall a. (Num (a -> a), Floating a) => a -> a
---
