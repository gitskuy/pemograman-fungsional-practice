import Geometry.Sphere as Sphere

sphereVolume :: Float
sphereVolume = Sphere.volume 3

--- >>> sphereVolume
--- 113.097336
---

