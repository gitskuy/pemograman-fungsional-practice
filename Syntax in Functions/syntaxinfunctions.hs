lucky :: (Integral a) => a -> String
lucky 7 = "LUCKY NUMBER SEVEN! "
lucky x = "Sorry you're out of luck, pal!"

--- >>> lucky 10
--- "Sorry you're out of luck, pal!"
---

sayMe :: (Integral a) => a -> String
sayMe 1 = "One!"
sayMe 2 = "Two!"
sayMe 3 = "Three!"
sayMe 4 = "Four!"
sayMe 5 = "Five!"
sayMe x = "Not between 1 and 5"

--- >>> sayMe 4
--- >>> sayMe 10
--- "Four!"
--- "Not between 1 and 5"

factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n -1)

--- >>> factorial 4
--- 24
--- recursivly



charName :: Char -> String
charName 'a' = "Albert"
charName 'b' = "Broseph"
charName 'c' = "Cecil"

--- >>> charName 'f'
--- *** Exception: /home/ahmad/Documents/SEMESTERTERAKHIRAMIN/PEMFUNG/BELAJARPEMFUNG/pemograman-fungsional-practice/Syntax in Functions/syntaxinfunctions.hs:(33,1)-(35,22): Non-exhaustive patterns in function charName
--- <BLANKLINE>
--- >>> charName 'b'
--- "Broseph"
---

addVectors :: (Num a) => (a,a) -> (a,a) -> (a,a)
addVectors a b = (fst a + fst b, snd a + snd b)

--- >>> addVectors (5,4) (4,5)
--- (9,9)
---

addVectors2 :: (Num a) => (a, a) -> (a, a) -> (a, a)
addVectors2 (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

--- >>> addVectors2 (5,4) (4,5)
--- (9,9)
---


first :: (a, b, c) -> a
first (x, _, _) = x

second :: (a, b, c) -> b
second (_, y, _) = y

third :: (a, b, c) -> c
third (_, _, z) = z

first4 :: (a, b, c,d) -> a
first4 (x, _, _,_) = x

--- >>> first (1,23,423,1231231)
--- 1
--- >>> first (1,23,423,1231231)
--- <interactive>:33:8: error:
---     • Couldn't match expected type ‘(a, b0, c0)’
---                   with actual type ‘(Integer, Integer, Integer, Integer)’
---     • In the first argument of ‘first’, namely ‘(1, 23, 423, 1231231)’
---       In the expression: first (1, 23, 423, 1231231)
---       In an equation for ‘it’: it = first (1, 23, 423, 1231231)
---     • Relevant bindings include it :: a (bound at <interactive>:33:2)
---
--- >>> first4 (1,2,3,4)
--- 1
---
xs:: [(Integer,Integer)]
xs = [(1,3), (4,3), (2,4), (5,3), (5,6), (3,1)]

xsSum :: [Integer]
xsSum = [a+b | (a,b) <- xs]
--- >>>  [a+b | (a,b) <- xs]
--- [4,7,6,8,11,4]
---
--- >>> xsSum
--- [4,7,6,8,11,4]
---

head' :: [a] -> a
head' [] = error "Can't call head on an empty list, dummy!"
head' (x:_) = x

--- >>> head' []
--- *** Exception: Can't call head on an empty list, dummy!
--- CallStack (from HasCallStack):
---   error, called at /home/ahmad/Documents/SEMESTERTERAKHIRAMIN/PEMFUNG/BELAJARPEMFUNG/pemograman-fungsional-practice/Syntax in Functions/syntaxinfunctions.hs:97:12 in main:Main
---

--- >>> head' "Hello"
--- 'H'
---

--- >>> head' [1,2,3]
--- 1
---

firstofall :: [a] -> a
firstofall' [] = error "Can't call head on an empty list, brother"
firstofall (x:_) = x

--- >>> firstofall [12 .. 14]
--- 12
---

tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
tell (x : []) = "The list has one element: " ++ show x
tell (x : y : []) = "The list has two element: " ++ show x ++  " and " ++ show y
tell (x : y : _ ) = "The list has lots of element: " ++ show x ++ " and " ++ show y ++ " and so on"


test :: [Integer]
test = [ x*y | x <- [2,5,10], y <- [8,10,11]]

--- >>> tell [1 .. 100]
--- "The list has lots of element: 1 and 2 and so on"
---
--- >>> tell test
--- "The list has lots of element: 16 and 20 and so on"
---



sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

--- >>> sum test
--- 493
---

capital :: String -> String
capital " " = "Empty string, whoops"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

--- >>> capital "Dracula"
--- "The first letter of Dracula is D"
--- >>> capital "life"
--- "The first letter of life is l"
---

---- GUARDS ----

bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | weight / height ^ 2 <= 18.5 = "You're underweight, you emo, you!"
    | weight / height ^ 2 <= 25.0 = "You're supposedly normal. Pffft, I bet you're ugly!"
    | weight / height ^ 2 <= 30.0 = "You're fat! Lose some weight, fatty!"
    | otherwise                 = "You're a whale, congratulations!"

--- >>> bmiTell 95 1.80
--- "You're fat! Lose some weight, fatty!"
---


max' :: (Ord a) => a -> a -> a
max' a b
    | a > b = a
    | otherwise = b

--- >>> max 5 10
--- 10
---

cutiMerry :: (Ord a) => a -> a -> a
cutiMerry a b | a < b = b | otherwise = a

--- >>> cutiMerry 10 12
--- 12
---

myCompare :: (Ord a) => a -> a -> Ordering
a `myCompare` b
    | a > b     = GT
    | a == b    = EQ
    | otherwise = LT

--- >>> 1 `myCompare` 2
--- LT
---

myCompare2 :: (Ord a) => a -> a -> String
a `myCompare2` b
    | a > b     = "GT"
    | a == b    = "EQ"
    | otherwise = "LT"

--- >>> "my" `myCompare` "123"
--- GT
---

bmiTell2 :: (RealFloat a) => a -> a -> String
bmiTell2 weight height
    | bmi <= skinny = "You are underweight"
    | bmi <= normal = "You are normal weight"
    | bmi <= fat    = "You are a bit fat"
    | otherwise     = "You are above average loose some weight please"
    where bmi = weight / height ^ 2
          (skinny,normal,fat) = (18.5,25.0,30.0)
--- >>> bmiTell2 95 1.80
--- "You are a bit fat"
---

initials :: String -> String -> String
initials firstname lastname = [f] ++ ". " ++ [l] ++ "."
        where (f:_) = firstname
              (l:_) = lastname

--- >>> initials "Ahmad" "Yazid"
--- "A. Y."
---
calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi | (w, h) <- xs, let bmi = w / h ^ 2 ,bmi >= 25.0]

--- >>> calcBmis [(180,1.90),(100,1.85)]
--- [49.86149584487535,29.218407596785973]
---


--- Let it be ---

cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
    let sideArea = 2 * pi * r * h
        topArea = pi * r ^ 2
    in  sideArea + 2 * topArea

--- >>> cylinder 10.0 40.0
--- 3141.5926535897934
---

--- >>> let zoot x y z = x * y + z
--- >>> zoot 3 9 2
--- >>> zoot 60

--- >>> let boot x y z = x * y + z in boot 3 4 2
--- 14
--- >>> boot
--- <interactive>:1507:2: error: Variable not in scope: boot
---

describeList :: [a] -> String
describeList xs = "The list is " ++ case xs of [] -> "empty."
                                               [x] -> "a singleton list."
                                               xs -> "a longer list."

--- >>> describeList [1 .. 50]
--- "The list is a longer list."
---

printList :: [a] -> String
printList xs = "List " ++ what xs
    where what [] = "empty, "
          what [x] = " one list"
          what xs = "Long list bro"

--- >>> printList [1, 2]
--- "List Long list bro"
---
