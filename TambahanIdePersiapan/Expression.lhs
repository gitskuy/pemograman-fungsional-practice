> data Expr = C Float | Expr :+ Expr | Expr :- Expr
>           | Expr :* Expr | Expr :/ Expr
>           | V [Char]
>           | Let String Expr Expr | Fold Expr Expr
>      deriving Show

> subst :: String -> Expr -> Expr -> Expr

> subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
> subst v0 e0 (C c)          = (C c)
> subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
> subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :- subst v0 e0 e2
> subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :* subst v0 e0 e2
> subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :/ subst v0 e0 e2
> subst v0 e0 (Let v1 e1 e2) = Let v0 e0 (subst v1 e1 e2)


> evaluate :: Expr -> Float
> evaluate (C x) = x
> evaluate (e1 :+ e2)    = evaluate e1 + evaluate e2
> evaluate (e1 :- e2)    = evaluate e1 - evaluate e2
> evaluate (e1 :* e2)    = evaluate e1 * evaluate e2
> evaluate (e1 :/ e2)    = evaluate e1 / evaluate e2
> evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
> evaluate (V v)         = evaluate (subst v (V v) (V v))

> mapex :: (Expr -> float) -> [Expr] -> [float]
> mapex _ [] = []
> mapex f (x:xs) = f x : mapex f xs


< foldex            :: (Expr -> float) -> Expr -> [Expr] -> [Expr -> float]
< foldex f z []     = z
< foldex f z [x]    = x                             -- aka foldt' of data-ordlist
< foldex f z xs     = foldt f z (pairs f xs)

--- >>> five = (C 5)
--- >>> three = (C 3)
--- >>> a = (V "a")
--- >>> fivefiplus  = (five :+ three)
--- >>> fivefiplus
--- >>> evaluate(fivefiplus)
--- >>> mapex (evaluate) [five,three,fivefiplus]
--- >>> 5 * 5


> maxTiga x y z = maxz (maxz x y) z
>                 where maxz x y = if x > y then x else y

--- >>> maxTiga 5 4 8
--- 8
---
