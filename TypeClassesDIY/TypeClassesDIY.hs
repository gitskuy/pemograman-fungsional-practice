
data Bool = False | True
data Shape = Circle Float Float Float | Rectangle Float Float Float Float | Triangle Float Float Float deriving (Show)

surface :: Shape -> Float
surface (Triangle alas1 alas2 tinggi) = ((alas1 * alas2) * tinggi) / 2
surface (Circle _ _ r) = pi * r ^ 2
surface (Rectangle x1 y1 x2 y2) = (abs $ x2 - x1) * (abs $ y2 -y1)


--- >>> :t Circle
--- Circle :: Float -> Float -> Float -> Shape
--- >>> surface $ Circle 10 20 10
--- 314.15927
--- >>> Circle 10 20 5
--- >>> surface $ Rectangle 0 0 100 1000
--- Circle 10.0 20.0 5.0
--- 100000.0
--- >>> Triangle 20 20 2
--- >>> map (Circle 10 20) [4,5,6,6]
--- Triangle 20.0 20.0 2.0
--- [Circle 10.0 20.0 4.0,Circle 10.0 20.0 5.0,Circle 10.0 20.0 6.0,Circle 10.0 20.0 6.0]
---


