data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     , height :: Float
                     , phoneNumber :: String
                     , flavor :: String
                     } deriving (Show)

guy :: Person
guy = Person "Buddy" "Finklestein" 43 184.2 "526-2928" "Chocolate"


--- Person "Buddy" "Finklestein" 43 184.2 "526-2928" "Chocolate"
--- >>> firstName guy
--- "Buddy"
---
--- >>> height guy
--- 184.2
--- >>> :t flavor
--- flavor :: Person -> String
---


