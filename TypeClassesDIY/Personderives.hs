data Person = Person { firstName :: String
                     , lastName :: String
                     , age :: Int
                     } deriving (Eq,Show,Read)

guy :: Person
guy = Person {firstName = "Michael Learns TO", lastName = "Diamond", age = 43}

guy2 :: Person
guy2 = Person {firstName = "Michael", lastName = "Diamond", age = 43}

--- Person "Buddy" "Finklestein" 43 184.2 "526-2928" "Chocolate"
--- >>> firstName guy
--- "Michael"
---
--- >>> guy == guy2
--- False
--- >>> show guy
--- "Person {firstName = \"Michael Learns TO\", lastName = \"Diamond\", age = 43}"
---

--- >>> read "Person {firstName =\"Michael\", lastName =\"Diamond\", age = 43}" :: Person
--- Person {firstName = "Michael", lastName = "Diamond", age = 43}
---


--- >>> read "Person {firstName =\"Michael\", lastName =\"Diamond\", age = 43}" == guy
--- False
---

data Day = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
           deriving (Eq, Ord, Show, Read, Bounded, Enum)
--- >>> Wednesday
--- Wednesday
--- >>> Monday > Tuesday
--- False
--- >>> read "Saturday" :: Day
--- Saturday
---

--- >>> Monday `compare` Wednesday
--- LT
---


--- >>> minBound :: Day
--- Monday
--- >>> maxBound :: Day
--- Sunday
---

--- >>> succ Monday
--- Tuesday
--- >>> pred Saturday
--- Friday
--- >>> [Thursday .. Sunday]
--- [Thursday,Friday,Saturday,Sunday]



phoneBook :: [(String,String)]
phoneBook =
    [("betty","555-2938")
    ,("bonnie","452-2928")
    ,("patsy","493-2928")
    ,("lucille","205-2928")
    ,("wendy","939-8282")
    ,("penny","853-2492")
    ]

--- >>> type PhoneBook
--- <interactive>:1499:16: error:
---     parse error (possibly incorrect indentation or mismatched brackets)
---


data Either a b = Left a | Right b deriving (Eq, Ord, Read, Show)

--- >>> Right 20
--- Right 20
--- >>> Left "w00t"
--- Left "w00t"
--- >>>  :t Right 'a'
--- Right 'a' :: Either a Char
---

import qualified Data.Map as Map

data LockerState = Taken | Free deriving (Show, Eq)

type Code = String

type LockerMap = Map.Map Int (LockerState, Code)

lockerLookup :: Int -> LockerMap -> Either String Code
lockerLookup lockerNumber map =
    case Map.lookup lockerNumber map of
        Nothing -> Left $ "Locker number " ++ show lockerNumber ++ " doesn't exist!"
        Just (state, code) -> if state /= Taken
                                then Right code
                                else Left $ "Locker " ++ show lockerNumber ++ " is already taken!"

lockers :: LockerMap
lockers = Map.fromList
    [(100,(Taken,"ZD39I"))
    ,(101,(Free,"JAH3I"))
    ,(103,(Free,"IQSA9"))
    ,(105,(Free,"QOTSA"))
    ,(109,(Taken,"893JJ"))
    ,(110,(Taken,"99292"))
    ]

--- >>> lockerLookup 101 lockers
--- <interactive>:1702:2: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: lockerLookup :: Integer -> t0 -> IO a0
--- <BLANKLINE>
--- <interactive>:1702:19: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: lockers
--- *** Exception: <interactive>:1702:2: error:
---     Variable not in scope: lockerLookup :: Integer -> t0 -> IO a0
--- (deferred type error)
---


data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)

singleton :: a -> Tree a
singleton x = Node x EmptyTree EmptyTree

treeInsert :: (Ord a) => a -> Tree a -> Tree a
treeInsert x EmptyTree = singleton x
treeInsert x (Node a left right)
    | x == a = Node x left right
    | x < a  = Node a (treeInsert x left) right
    | x > a  = Node a left (treeInsert x right)


treeElem :: (Ord a) => a -> Tree a -> Bool
treeElem x EmptyTree = False
treeElem x (Node a left right)
    | x == a = True
    | x < a  = treeElem x left
    | x > a  = treeElem x right


nums = [8,6,4,1,7,3,5]
numsTree = foldr treeInsert EmptyTree nums
--- >>> numsTree
--- <interactive>:1844:2: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: numsTree :: IO a0
--- *** Exception: <interactive>:1844:2: error:
---     Variable not in scope: numsTree :: IO a0
--- (deferred type error)
---

