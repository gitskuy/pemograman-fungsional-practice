maximum' :: (Ord a) => [a] -> a
maximum' [] = error "maximum of empty list"
maximum' [x] = x
maximum' (x:xs)
    | x > maxTail = x
    | otherwise = maxTail
    where maxTail = maximum' xs


maxim :: (Ord a) => [a] -> a
maxim [] = error "empty bruf"
maxim [x] = x
maxim (x:xs) = max x (maxim xs)



sumz :: (Integer a) => [a] -> a
sumz [] = error "sum empty list none"
sumz [x] = x
sumz (x:xs) = sum x + (sum xs)



replic :: (Num i, Ord i) => i -> a -> [a]
replic n x
  | n <= 0    = []
  | otherwise = x:replic (n - 1) x


ngulang :: (Num i, Ord i) => i -> a -> [a]
ngulang n x
    | n <= 0    = []
    | otherwise = x:ngulang (n-1) x



take' :: (Num i, Ord i) => i -> [a] -> [a]
take' n _
    | n <= 0   = []
take' _ []     = []
take' n (x:xs) = x : take' (n-1) xs


balik :: (String a) => [a] -> [a]
balik [] = []
balik (x:xs) = balik xs ++ [x]

--- >>> balik ["Aye","nama"]
--- <interactive>:269:2: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: balik :: [[Char]] -> IO a0
--- *** Exception: <interactive>:269:2: error:
---     Variable not in scope: balik :: [[Char]] -> IO a0
--- (deferred type error)
---

x = [ (x,y) | x <- [1..3] , y <- [1..(2 * x)] ]

-- >>> x
-- <interactive>:183:2: warning: [-Wdeferred-out-of-scope-variables]
--     Variable not in scope: x :: IO a0
-- *** Exception: <interactive>:183:2: error: Variable not in scope: x :: IO a0
-- (deferred type error)
--
