balik :: [a] -> [a]
balik [] = []
balik (x:xs) = balik xs ++ [x]

--- >>> balik [1,2,3]
--- [3,2,1]
---

ngulang :: a -> [a]
ngulang x = x:ngulang x
--- >>> take 4 (ngulang 3)
--- [3,3,3,3]
---

zip' :: [a] -> [b] -> [(a,b)]
zip' _ [] = []
zip' [] _ = []
zip' (x:xs) (y:ys) = (x,y):zip' xs ys

--- >>> zip' [1,2,4] [1,1,2]
--- [(1,1),(2,1),(4,2)]
---


elem' :: (Eq a) => a -> [a] -> Bool
elem' a [] = False
elem' a (x:xs)
    | a == x    = True
    | otherwise = a `elem'` xs

--- >>> elem' 5 [1,2,3,4,5]
--- True
---

quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) =
    let smallerSorted = quicksort [a | a <- xs, a <= x]
        biggerSorted = quicksort [a | a <- xs, a > x]
    in  smallerSorted ++ [x] ++ biggerSorted

--- >>> quicksort [1.2,3,8,7,4]
--- [1.2,3.0,4.0,7.0,8.0]
---
