> module Trees where

-- This code was automatically extracted from a .lhs file that
-- uses the following convention:

-- lines beginning with ">" are executable
-- lines beginning with "<" are in the text,
     but not necessarily executable
-- lines beginning with "|" are also in the text,
     but are often just expressions or code fragments.

> data Shape = Rectangle Side Side
>            | Ellipse Radius Radius
>            | RtTriangle Side Side
>            | Polygon [Vertex]
>      deriving Show
>
> type Radius = Float
> type Side   = Float
> type Vertex  = (Float,Float)

> data List a = Nil | MkList a (List a)

< Nil    :: List a
< MkList :: a -> List a -> List a

< []  :: [a]
< (:) :: a -> [a] -> [a]

> data Tree a = Leaf a | Branch (Tree a) (Tree a)

--- >>> treex
--- <interactive>:2080:2: error: Variable not in scope: treex
---

< data IntegerTree = Leaf Integer | Branch IntegerTree IntegerTree deriving(Show,Enum,Eq)

--- >>> what
--- <interactive>:2410:2: error: Variable not in scope: what
---

< data SimpleTree  = Leaf | Branch SimpleTree SimpleTree

--- >>> IntegerTree = Leaf 5
--- <interactive>:1904:2: error:
---     Not in scope: data constructor ‘IntegerTree’
---


> data InternalTree a = ILeaf
>                     | IBranch a (InternalTree a) (InternalTree a)

> data FancyTree a b = FLeaf a
>                    | FBranch b (FancyTree a b) (FancyTree a b)

> mapTree :: (a->b) -> Tree a -> Tree b

> mapTree f (Leaf x)       = Leaf (f x)
> mapTree f (Branch t1 t2) = Branch (mapTree f t1)
>                                    (mapTree f t2)

> fringe               :: Tree a -> [a]
> fringe (Leaf x)       = [x]
> fringe (Branch t1 t2) = fringe t1 ++ fringe t2

> treeSize               :: Tree a -> Integer
> treeSize (Leaf _)       = 1
> treeSize (Branch t1 t2) = treeSize t1 + treeSize t2

> treeHeight		   :: Tree a -> Integer
> treeHeight (Leaf _)       = 0
> treeHeight (Branch t1 t2) = 1 + max (treeHeight t1)
>                                     (treeHeight t2)

< data Expr = C Float | Add Expr Expr | Sub Expr Expr
<           | Mul Expr Expr | Div Expr Expr



--- >>> Tree x
--- <interactive>:745:2: error:
---     • Data constructor not in scope: Tree :: t0 -> t
---     • Perhaps you meant ‘True’ (imported from Prelude)
--- <BLANKLINE>
--- <interactive>:745:7: error: Variable not in scope: x
---

> data Expr = C Float | Expr :+ Expr | Expr :- Expr
>           | Expr :* Expr | Expr :/ Expr
>           | V [Char]
>           | Let String Expr Expr | Fold Expr Expr
>      deriving Show

mapExpr =
-- >>> (C 10.0 :+ (C 8 :/ C 2)) :* (C 7 :- C 4)
-- (C 10.0 :+ (C 8.0 :/ C 2.0)) :* (C 7.0 :- C 4.0)
-- >>> five = (C 5)

> subst :: String -> Expr -> Expr -> Expr

> subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
> subst v0 e0 (C c)          = (C c)
> subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
> subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :- subst v0 e0 e2
> subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :* subst v0 e0 e2
> subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :/ subst v0 e0 e2
> subst v0 e0 (Let v1 e1 e2) = Let v0 e0 (subst v1 e1 e2)

--- >>> expGua = (Let "AYE" (C 5) (Let "TEPE" (C 7) (C 8)))
--- >>> evaluate expGua
--- >>> expGua
--- >>> expgue1 = subst "AJI" (C 2) expGua
--- >>> expgue1
--- <interactive>:2691:12: error:
---     • Data constructor not in scope: Let :: [Char] -> t0 -> t1 -> t
---     • Perhaps you meant ‘Left’ (imported from Prelude)
--- <BLANKLINE>
--- <interactive>:2691:23: error:
---     Data constructor not in scope: C :: Integer -> t0
--- <BLANKLINE>
--- <interactive>:2691:29: error:
---     • Data constructor not in scope: Let :: [Char] -> t2 -> t3 -> t1
---     • Perhaps you meant ‘Left’ (imported from Prelude)
--- <BLANKLINE>
--- <interactive>:2691:41: error:
---     Data constructor not in scope: C :: Integer -> t2
--- <BLANKLINE>
--- <interactive>:2691:47: error:
---     Data constructor not in scope: C :: Integer -> t3
--- <BLANKLINE>
--- <interactive>:2692:2: error:
---     Variable not in scope: evaluate :: t0 -> t
--- <BLANKLINE>
--- <interactive>:2692:11: error: Variable not in scope: expGua
--- <BLANKLINE>
--- <interactive>:2693:2: error: Variable not in scope: expGua
--- <BLANKLINE>
--- <interactive>:2694:12: error:
---     Variable not in scope: subst :: [Char] -> t0 -> t1 -> t
--- <BLANKLINE>
--- <interactive>:2694:25: error:
---     Data constructor not in scope: C :: Integer -> t0
--- <BLANKLINE>
--- <interactive>:2694:30: error: Variable not in scope: expGua
--- <BLANKLINE>
--- <interactive>:2695:2: error: Variable not in scope: expgue1
---

--- >>> expTree = (C 5)
--- <interactive>:2687:13: error:
---     Data constructor not in scope: C :: Integer -> t
---

--- >>> subst

--- Diskusi bersama teman teman AJI VITO DAN ALIF DAN AJI

--- 8.0
--- Let "AYE" (C 5.0) (Let "AJI" (C 7.0) (C 8.0))
--- expGua :: Expr
--- >>> subst "TEST" (C 5) ((C 8) :+ (C 5))
--- >>> evaluate(test)
--- C 8.0 :+ C 5.0
--- 13.0
--- >>> subst "TEST" expGua expGua
--- <interactive>:2331:15: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: expGua :: Expr
--- <BLANKLINE>
--- <interactive>:2331:15: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: expGua :: Expr
--- <BLANKLINE>
--- <interactive>:2331:22: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: expGua :: Expr
--- <BLANKLINE>
--- <interactive>:2331:22: warning: [-Wdeferred-out-of-scope-variables]
---     Variable not in scope: expGua :: Expr
--- *** Exception: <interactive>:2331:22: error: Variable not in scope: expGua :: Expr
--- (deferred type error)
---


> evaluate :: Expr -> Float

> evaluate (C x) = x
> evaluate (e1 :+ e2)    = evaluate e1 + evaluate e2
> evaluate (e1 :- e2)    = evaluate e1 - evaluate e2
> evaluate (e1 :* e2)    = evaluate e1 * evaluate e2
> evaluate (e1 :/ e2)    = evaluate e1 / evaluate e2
> evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
> evaluate (V v)         = 0.0


-- >>> exp0 = (((C 5.0) :+ (C 7) :* (C 4)))
-- >>> exp0
-- (C 5.0 :+ C 7.0) :* C 4.0
--


--- >>> subtract


-- >>> evaluate exp0
-- 48.0
--


-- >>> evaluate (((C 5) :+ (C 7) :* (C 4)))
-- 48.0
--
-- >>> exp1 = Let "x" (C 7) ((C 5) :+ (C 7) :* (V "x"))
-- >>> exp1
-- >>> evaluate exp1
-- Let "x" (C 7.0) ((C 5.0 :+ C 7.0) :* V "x")
-- 19.0
--

-- >>> evaluate exp1
-- 19.0
--


-- >>> evaluate (Let "x" (C 5) (V "x" :+ (C 7)))
-- 12.0
--

> simple n a b = n * (a-b)

-- >>> fun1 = simple 5 2
-- >>> fun1 3
-- 25
--
-- >>> fun1 7
-- 45
--

> myflip f x y = f y x

> newSimple = myflip simple

> newSimple2 n = myflip (simple n)

-- >>> :t newSimple
-- newSimple :: Integer -> Integer -> Integer -> Integer
--

-- >>> newSimple 5 2 3
-- 4

-- >>> simple 5 2 7
-- -25
--

-- >>> newSimple2 5 2 3
-- 5
--

-- >>> simple 5 2 3
-- >>> fun0 = myflip simple n a
-- -5
-- <BLANKLINE>
-- <interactive>:50:23: warning: [-Wdeferred-out-of-scope-variables]
--     Variable not in scope: n
-- <BLANKLINE>
-- <interactive>:50:25: warning: [-Wdeferred-out-of-scope-variables]
--     Variable not in scope: a
--