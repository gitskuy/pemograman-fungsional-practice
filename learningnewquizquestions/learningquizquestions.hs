--- KPK Implementasi ---
kpk :: Integer -> Integer -> Integer
kpk x y = [z | z <- [x,x+x .. ], z `mod` y == 0] !! 0

--- >>> kpk 20 2
--- 20

--- >>> [(x,y) | x <- [1..4],y <- [2 .. 6] , x * 2 == y]
--- [(1,2),(2,4),(3,6)]
---

--- takes only the  one that satisfies x * 2 == y

maxTiga :: (Num a , Ord a) => a -> a -> a -> a
maxTiga x y z = maxz (maxz x y) (z)
    where maxz x y = if x > y then x else y

--- >>> maxTiga 2 2 4
--- >>> maxTiga 5 20 9
--- 4
--- 20
---

quicksort2 :: (Ord a) => [a]-> [a]
quicksort2 [] = []
quicksort2 (x:xs) = smallSort ++ [x] ++ bigSort
    where smallSort = quicksort2 (filter (<=x) xs)
          bigSort = quicksort2 (filter (>x) xs)


--- >>> quicksort2 [1,2,3]
--- [1,2,3]
---

--- learning with shuklan.com

guessMyNumber x
            | x > 27    = "Too high!"
            | x < 27    = "Too low!"
            | otherwise = "Correct!"



--- >>> guessMyNumber 4
--- "Too low!"
---


convert :: (Double, [Char]) -> (Double, [Char])
convert (x,y)
        | (y == "m") = (x * 1.09361,"yd")
        | (y == "L")  = (x * 0.2641,"gal")
        | (y == "kg") = (x * 2.2,"kg")

--- >>> convert(1,"m")
--- (1.09361,"yd")
--- >>> foldl (+) 0 [1, 2, 3]
--- 6
---

pythagoras = [(x,y,z) | z <- [1 ..],y <- [1..z], x <- [1..y] , z^2 == x^2 + y^2]

-- >>> take 4 pyt

factorial x = foldr (*) 1 [1..x]
permutations x k = (factorial x) / (factorial (x-k))

maxList xs = foldr (max) 1 xs

