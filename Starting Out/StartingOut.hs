boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]

--- >>> boomBangs [7..13]
--- ["BOOM!","BOOM!","BANG!","BANG!"]
---


test = [ x*y | x <- [2,5,10], y <- [8,10,11]]

--- >>> test
--- [16,20,22,40,50,55,80,100,110]
---

length' xs = sum [1 | _ <- xs]

--- >>> length [1..120]
--- 120
---
removeNonUppercase :: [Char] -> [Char]
removeNonUppercase st = [ c | c <- st, c `elem` ['A'..'Z']]
--- >>> removeNonUppercase "IdontLIKEFROGS"
--- "ILIKEFROGS"
--- >>> :t removeNonUppercase
--- removeNonUppercase :: [Char] -> [Char]
---

xxs = [[1,3,5,2,3,1,2,4,5],[1,2,3,4,5,6,7,8,9],[1,2,4,2,1,6,3,1,3,2,3,6]]

--- >>> [ [ x | x <- xs, even x ] | xs <- xxs]
--- [[2,2,4],[2,4,6,8],[2,4,2,6,2,6]]
---

oneto5tuple = zip [1 .. 5] ["one", "two", "three", "four", "five"]

--- >>> oneto5tuple
--- [(1,"one"),(2,"two"),(3,"three"),(4,"four"),(5,"five")]
---

applesoranges = zip [1..] ["apple", "orange", "cherry", "mango"]

--- >>> applesoranges
--- [(1,"apple"),(2,"orange"),(3,"cherry"),(4,"mango")]
---

triangles = [ (a,b,c) | c <- [1..10], b <- [1..10], a <- [1..10] ]

--- >>> triangles
--- [(1,1,1),(2,1,1),(3,1,1),(4,1,1),(5,1,1),(6,1,1),(7,1,1),(8,1,1),(9,1,1),(10,1,1),(1,2,1),(2,2,1),(3,2,1),(4,2,1),(5,2,1),(6,2,1),(7,2,1),(8,2,1),(9,2,1),(10,2,1),(1,3,1),(2,3,1),(3,3,1),(4,3,1),(5,3,1),(6,3,1),(7,3,1),(8,3,1),(9,3,1),(10,3,1),(1,4,1),(2,4,1),(3,4,1),(4,4,1),(5,4,1),(6,4,1),(7,4,1),(8,4,1),(9,4,1),(10,4,1),(1,5,1),(2,5,1),(3,5,1),(4,5,1),(5,5,1),(6,5,1),(7,5,1),(8,5,1),(9,5,1),(10,5,1),(1,6,1),(2,6,1),(3,6,1),(4,6,1),(5,6,1),(6,6,1),(7,6,1),(8,6,1),(9,6,1),(10,6,1),(1,7,1),(2,7,1),(3,7,1),(4,7,1),(5,7,1),(6,7,1),(7,7,1),(8,7,1),(9,7,1),(10,7,1),(1,8,1),(2,8,1),(3,8,1),(4,8,1),(5,8,1),(6,8,1),(7,8,1),(8,8,1),(9,8,1),(10,8,1),(1,9,1),(2,9,1),(3,9,1),(4,9,1),(5,9,1),(6,9,1),(7,9,1),(8,9,1),(9,9,1),(10,9,1),(1,10,1),(2,10,1),(3,10,1),(4,10,1),(5,10,1),(6,10,1),(7,10,1),(8,10,1),(9,10,1),(10,10,1),(1,1,2),(2,1,2),(3,1,2),(4,1,2),(5,1,2),(6,1,2),(7,1,2),(8,1,2),(9,1,2),(10,1,2),(1,2,2),(2,2,2),(3,2,2),(4,2,2),(5,2,2),(6,2,2),(7,2,2),(8,2,2),(9,2,2),(10,2,2),(1,3,2),(2,3,2),(3,3,2),(4,3,2),(5,3,2),(6,3,2),(7,3,2),(8,3,2),(9,3,2),(10,3,2),(1,4,2),(2,4,2),(3,4,2),(4,4,2),(5,4,2),(6,4,2),(7,4,2),(8,4,2),(9,4,2),(10,4,2),(1,5,2),(2,5,2),(3,5,2),(4,5,2),(5,5,2),(6,5,2),(7,5,2),(8,5,2),(9,5,2),(10,5,2),(1,6,2),(2,6,2),(3,6,2),(4,6,2),(5,6,2),(6,6,2),(7,6,2),(8,6,2),(9,6,2),(10,6,2),(1,7,2),(2,7,2),(3,7,2),(4,7,2),(5,7,2),(6,7,2),(7,7,2),(8,7,2),(9,7,2),(10,7,2),(1,8,2),(2,8,2),(3,8,2),(4,8,2),(5,8,2),(6,8,2),(7,8,2),(8,8,2),(9,8,2),(10,8,2),(1,9,2),(2,9,2),(3,9,2),(4,9,2),(5,9,2),(6,9,2),(7,9,2),(8,9,2),(9,9,2),(10,9,2),(1,10,2),(2,10,2),(3,10,2),(4,10,2),(5,10,2),(6,10,2),(7,10,2),(8,10,2),(9,10,2),(10,10,2),(1,1,3),(2,1,3),(3,1,3),(4,1,3),(5,1,3),(6,1,3),(7,1,3),(8,1,3),(9,1,3),(10,1,3),(1,2,3),(2,2,3),(3,2,3),(4,2,3),(5,2,3),(6,2,3),(7,2,3),(8,2,3),(9,2,3),(10,2,3),(1,3,3),(2,3,3),(3,3,3),(4,3,3),(5,3,3),(6,3,3),(7,3,3),(8,3,3),(9,3,3),(10,3,3),(1,4,3),(2,4,3),(3,4,3),(4,4,3),(5,4,3),(6,4,3),(7,4,3),(8,4,3),(9,4,3),(10,4,3),(1,5,3),(2,5,3),(3,5,3),(4,5,3),(5,5,3),(6,5,3),(7,5,3),(8,5,3),(9,5,3),(10,5,3),(1,6,3),(2,6,3),(3,6,3),(4,6,3),(5,6,3),(6,6,3),(7,6,3),(8,6,3),(9,6,3),(10,6,3),(1,7,3),(2,7,3),(3,7,3),(4,7,3),(5,7,3),(6,7,3),(7,7,3),(8,7,3),(9,7,3),(10,7,3),(1,8,3),(2,8,3),(3,8,3),(4,8,3),(5,8,3),(6,8,3),(7,8,3),(8,8,3),(9,8,3),(10,8,3),(1,9,3),(2,9,3),(3,9,3),(4,9,3),(5,9,3),(6,9,3),(7,9,3),(8,9,3),(9,9,3),(10,9,3),(1,10,3),(2,10,3),(3,10,3),(4,10,3),(5,10,3),(6,10,3),(7,10,3),(8,10,3),(9,10,3),(10,10,3),(1,1,4),(2,1,4),(3,1,4),(4,1,4),(5,1,4),(6,1,4),(7,1,4),(8,1,4),(9,1,4),(10,1,4),(1,2,4),(2,2,4),(3,2,4),(4,2,4),(5,2,4),(6,2,4),(7,2,4),(8,2,4),(9,2,4),(10,2,4),(1,3,4),(2,3,4),(3,3,4),(4,3,4),(5,3,4),(6,3,4),(7,3,4),(8,3,4),(9,3,4),(10,3,4),(1,4,4),(2,4,4),(3,4,4),(4,4,4),(5,4,4),(6,4,4),(7,4,4),(8,4,4),(9,4,4),(10,4,4),(1,5,4),(2,5,4),(3,5,4),(4,5,4),(5,5,4),(6,5,4),(7,5,4),(8,5,4),(9,5,4),(10,5,4),(1,6,4),(2,6,4),(3,6,4),(4,6,4),(5,6,4),(6,6,4),(7,6,4),(8,6,4),(9,6,4),(10,6,4),(1,7,4),(2,7,4),(3,7,4),(4,7,4),(5,7,4),(6,7,4),(7,7,4),(8,7,4),(9,7,4),(10,7,4),(1,8,4),(2,8,4),(3,8,4),(4,8,4),(5,8,4),(6,8,4),(7,8,4),(8,8,4),(9,8,4),(10,8,4),(1,9,4),(2,9,4),(3,9,4),(4,9,4),(5,9,4),(6,9,4),(7,9,4),(8,9,4),(9,9,4),(10,9,4),(1,10,4),(2,10,4),(3,10,4),(4,10,4),(5,10,4),(6,10,4),(7,10,4),(8,10,4),(9,10,4),(10,10,4),(1,1,5),(2,1,5),(3,1,5),(4,1,5),(5,1,5),(6,1,5),(7,1,5),(8,1,5),(9,1,5),(10,1,5),(1,2,5),(2,2,5),(3,2,5),(4,2,5),(5,2,5),(6,2,5),(7,2,5),(8,2,5),(9,2,5),(10,2,5),(1,3,5),(2,3,5),(3,3,5),(4,3,5),(5,3,5),(6,3,5),(7,3,5),(8,3,5),(9,3,5),(10,3,5),(1,4,5),(2,4,5),(3,4,5),(4,4,5),(5,4,5),(6,4,5),(7,4,5),(8,4,5),(9,4,5),(10,4,5),(1,5,5),(2,5,5),(3,5,5),(4,5,5),(5,5,5),(6,5,5),(7,5,5),(8,5,5),(9,5,5),(10,5,5),(1,6,5),(2,6,5),(3,6,5),(4,6,5),(5,6,5),(6,6,5),(7,6,5),(8,6,5),(9,6,5),(10,6,5),(1,7,5),(2,7,5),(3,7,5),(4,7,5),(5,7,5),(6,7,5),(7,7,5),(8,7,5),(9,7,5),(10,7,5),(1,8,5),(2,8,5),(3,8,5),(4,8,5),(5,8,5),(6,8,5),(7,8,5),(8,8,5),(9,8,5),(10,8,5),(1,9,5),(2,9,5),(3,9,5),(4,9,5),(5,9,5),(6,9,5),(7,9,5),(8,9,5),(9,9,5),(10,9,5),(1,10,5),(2,10,5),(3,10,5),(4,10,5),(5,10,5),(6,10,5),(7,10,5),(8,10,5),(9,10,5),(10,10,5),(1,1,6),(2,1,6),(3,1,6),(4,1,6),(5,1,6),(6,1,6),(7,1,6),(8,1,6),(9,1,6),(10,1,6),(1,2,6),(2,2,6),(3,2,6),(4,2,6),(5,2,6),(6,2,6),(7,2,6),(8,2,6),(9,2,6),(10,2,6),(1,3,6),(2,3,6),(3,3,6),(4,3,6),(5,3,6),(6,3,6),(7,3,6),(8,3,6),(9,3,6),(10,3,6),(1,4,6),(2,4,6),(3,4,6),(4,4,6),(5,4,6),(6,4,6),(7,4,6),(8,4,6),(9,4,6),(10,4,6),(1,5,6),(2,5,6),(3,5,6),(4,5,6),(5,5,6),(6,5,6),(7,5,6),(8,5,6),(9,5,6),(10,5,6),(1,6,6),(2,6,6),(3,6,6),(4,6,6),(5,6,6),(6,6,6),(7,6,6),(8,6,6),(9,6,6),(10,6,6),(1,7,6),(2,7,6),(3,7,6),(4,7,6),(5,7,6),(6,7,6),(7,7,6),(8,7,6),(9,7,6),(10,7,6),(1,8,6),(2,8,6),(3,8,6),(4,8,6),(5,8,6),(6,8,6),(7,8,6),(8,8,6),(9,8,6),(10,8,6),(1,9,6),(2,9,6),(3,9,6),(4,9,6),(5,9,6),(6,9,6),(7,9,6),(8,9,6),(9,9,6),(10,9,6),(1,10,6),(2,10,6),(3,10,6),(4,10,6),(5,10,6),(6,10,6),(7,10,6),(8,10,6),(9,10,6),(10,10,6),(1,1,7),(2,1,7),(3,1,7),(4,1,7),(5,1,7),(6,1,7),(7,1,7),(8,1,7),(9,1,7),(10,1,7),(1,2,7),(2,2,7),(3,2,7),(4,2,7),(5,2,7),(6,2,7),(7,2,7),(8,2,7),(9,2,7),(10,2,7),(1,3,7),(2,3,7),(3,3,7),(4,3,7),(5,3,7),(6,3,7),(7,3,7),(8,3,7),(9,3,7),(10,3,7),(1,4,7),(2,4,7),(3,4,7),(4,4,7),(5,4,7),(6,4,7),(7,4,7),(8,4,7),(9,4,7),(10,4,7),(1,5,7),(2,5,7),(3,5,7),(4,5,7),(5,5,7),(6,5,7),(7,5,7),(8,5,7),(9,5,7),(10,5,7),(1,6,7),(2,6,7),(3,6,7),(4,6,7),(5,6,7),(6,6,7),(7,6,7),(8,6,7),(9,6,7),(10,6,7),(1,7,7),(2,7,7),(3,7,7),(4,7,7),(5,7,7),(6,7,7),(7,7,7),(8,7,7),(9,7,7),(10,7,7),(1,8,7),(2,8,7),(3,8,7),(4,8,7),(5,8,7),(6,8,7),(7,8,7),(8,8,7),(9,8,7),(10,8,7),(1,9,7),(2,9,7),(3,9,7),(4,9,7),(5,9,7),(6,9,7),(7,9,7),(8,9,7),(9,9,7),(10,9,7),(1,10,7),(2,10,7),(3,10,7),(4,10,7),(5,10,7),(6,10,7),(7,10,7),(8,10,7),(9,10,7),(10,10,7),(1,1,8),(2,1,8),(3,1,8),(4,1,8),(5,1,8),(6,1,8),(7,1,8),(8,1,8),(9,1,8),(10,1,8),(1,2,8),(2,2,8),(3,2,8),(4,2,8),(5,2,8),(6,2,8),(7,2,8),(8,2,8),(9,2,8),(10,2,8),(1,3,8),(2,3,8),(3,3,8),(4,3,8),(5,3,8),(6,3,8),(7,3,8),(8,3,8),(9,3,8),(10,3,8),(1,4,8),(2,4,8),(3,4,8),(4,4,8),(5,4,8),(6,4,8),(7,4,8),(8,4,8),(9,4,8),(10,4,8),(1,5,8),(2,5,8),(3,5,8),(4,5,8),(5,5,8),(6,5,8),(7,5,8),(8,5,8),(9,5,8),(10,5,8),(1,6,8),(2,6,8),(3,6,8),(4,6,8),(5,6,8),(6,6,8),(7,6,8),(8,6,8),(9,6,8),(10,6,8),(1,7,8),(2,7,8),(3,7,8),(4,7,8),(5,7,8),(6,7,8),(7,7,8),(8,7,8),(9,7,8),(10,7,8),(1,8,8),(2,8,8),(3,8,8),(4,8,8),(5,8,8),(6,8,8),(7,8,8),(8,8,8),(9,8,8),(10,8,8),(1,9,8),(2,9,8),(3,9,8),(4,9,8),(5,9,8),(6,9,8),(7,9,8),(8,9,8),(9,9,8),(10,9,8),(1,10,8),(2,10,8),(3,10,8),(4,10,8),(5,10,8),(6,10,8),(7,10,8),(8,10,8),(9,10,8),(10,10,8),(1,1,9),(2,1,9),(3,1,9),(4,1,9),(5,1,9),(6,1,9),(7,1,9),(8,1,9),(9,1,9),(10,1,9),(1,2,9),(2,2,9),(3,2,9),(4,2,9),(5,2,9),(6,2,9),(7,2,9),(8,2,9),(9,2,9),(10,2,9),(1,3,9),(2,3,9),(3,3,9),(4,3,9),(5,3,9),(6,3,9),(7,3,9),(8,3,9),(9,3,9),(10,3,9),(1,4,9),(2,4,9),(3,4,9),(4,4,9),(5,4,9),(6,4,9),(7,4,9),(8,4,9),(9,4,9),(10,4,9),(1,5,9),(2,5,9),(3,5,9),(4,5,9),(5,5,9),(6,5,9),(7,5,9),(8,5,9),(9,5,9),(10,5,9),(1,6,9),(2,6,9),(3,6,9),(4,6,9),(5,6,9),(6,6,9),(7,6,9),(8,6,9),(9,6,9),(10,6,9),(1,7,9),(2,7,9),(3,7,9),(4,7,9),(5,7,9),(6,7,9),(7,7,9),(8,7,9),(9,7,9),(10,7,9),(1,8,9),(2,8,9),(3,8,9),(4,8,9),(5,8,9),(6,8,9),(7,8,9),(8,8,9),(9,8,9),(10,8,9),(1,9,9),(2,9,9),(3,9,9),(4,9,9),(5,9,9),(6,9,9),(7,9,9),(8,9,9),(9,9,9),(10,9,9),(1,10,9),(2,10,9),(3,10,9),(4,10,9),(5,10,9),(6,10,9),(7,10,9),(8,10,9),(9,10,9),(10,10,9),(1,1,10),(2,1,10),(3,1,10),(4,1,10),(5,1,10),(6,1,10),(7,1,10),(8,1,10),(9,1,10),(10,1,10),(1,2,10),(2,2,10),(3,2,10),(4,2,10),(5,2,10),(6,2,10),(7,2,10),(8,2,10),(9,2,10),(10,2,10),(1,3,10),(2,3,10),(3,3,10),(4,3,10),(5,3,10),(6,3,10),(7,3,10),(8,3,10),(9,3,10),(10,3,10),(1,4,10),(2,4,10),(3,4,10),(4,4,10),(5,4,10),(6,4,10),(7,4,10),(8,4,10),(9,4,10),(10,4,10),(1,5,10),(2,5,10),(3,5,10),(4,5,10),(5,5,10),(6,5,10),(7,5,10),(8,5,10),(9,5,10),(10,5,10),(1,6,10),(2,6,10),(3,6,10),(4,6,10),(5,6,10),(6,6,10),(7,6,10),(8,6,10),(9,6,10),(10,6,10),(1,7,10),(2,7,10),(3,7,10),(4,7,10),(5,7,10),(6,7,10),(7,7,10),(8,7,10),(9,7,10),(10,7,10),(1,8,10),(2,8,10),(3,8,10),(4,8,10),(5,8,10),(6,8,10),(7,8,10),(8,8,10),(9,8,10),(10,8,10),(1,9,10),(2,9,10),(3,9,10),(4,9,10),(5,9,10),(6,9,10),(7,9,10),(8,9,10),(9,9,10),(10,9,10),(1,10,10),(2,10,10),(3,10,10),(4,10,10),(5,10,10),(6,10,10),(7,10,10),(8,10,10),(9,10,10),(10,10,10)]
---

rightTriangles' = [ (a,b,c) | c <- [1..10], b <- [1..c], a <- [1..b], a^2 + b^2 == c^2, a+b+c == 24]

--- >>> rightTriangles'
--- [(6,8,10)]


--- >>> :t "a"
--- "a" :: [Char]
---

addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z

--- >>> addThree 4 5 6
--- 15
--- >>> :t addThree
--- addThree :: Int -> Int -> Int -> Int
---



--- Type And Typeclasees

factorial :: Integer -> Integer
factorial n = product [1..n]

--- >>> factorial 50
--- 30414093201713378043612608166064768844377641568960512000000000000
---

circumfrence :: Float -> Float
circumfrence r = 2 * pi * r

--- >>> circumfrence 4.0
--- 25.132742
---

circumfrenceDouble :: Double -> Double
circumfrenceDouble r = 2 * pi * r

--- >>> circumfrenceDouble 4.0
--- 25.132741228718345
---

youGood :: Char -> Bool
youGood x = False

--- >>> youGood 'a'
--- False
---


--- Typeclasees

--- >>> :t (==)
--- (==) :: Eq a => a -> a -> Bool
--- a is generic  " Because it's not in capital case it's actually a type variable. That means that a can be of any type. This is much like generics in other languages, only in Haskell it's much more powerful because it allows us to easily write very general functions if they don't use any specific behavior of the types in them. Functions that have type variables are called polymorphic functions"
--- Eq is used for types that support equality testing.

--- >>> :t fst
--- fst :: (a, b) -> a
--- >>> fst (4,5)
--- 4
---



--- >>> :t (>)
--- (>) :: Ord a => a -> a -> Bool
--- "Ord covers all the standard comparing functions such as >, <, >= and <=. The compare function takes two Ord members of the same type and returns an ordering. Ordering is a type that can be GT, LT or EQ, meaning greater than, lesser than and equal, respectively."

--- >>> "Abrakadabra" < "Zebra"
--- True
---

--- >>> show 5
--- "5"
--- >>> read "5" + 4
--- 9
---

--- >>> :t read
--- read :: Read a => String -> a
---

--- >>> minBound :: Int
--- -9223372036854775808
---
